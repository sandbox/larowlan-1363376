<?php
/*
 * @file uc_restrict.module
 * Provides core module hooks etc
 * @copyright Copyright(c) 2010 Lee Rowlands
 * @license GPL v3 http://www.fsf.org/licensing/licenses/gpl.html
 * @author Lee Rowlands leerowlands at rowlands-bcs dot com
 *
 */


/**
 * Implementation of hook_perm
*/
function uc_restrict_permermission() {
  return array('Configure uc_restrict' => array('title' => t('Configure UC_restrict')));
}

/**
 * Form builder for settings form
*/
function uc_restrict_settings_form($form, $form_state) {
  $form['intro'] = array(
    '#value' => 'Use this page to setup the vocabulary to use for restricting product sales'
  );

  $vocabs = taxonomy_get_vocabularies();
  $options = array();

  foreach ($vocabs as $vocab) {
    $options[$vocab->vid] = check_plain($vocab->name);
  }

  $form['uc_restrict_vid'] = array(
    '#type' => 'select',
    '#title' => t('Taxonomy Vocabulary'),
    '#default_value' => variable_get('uc_restrict_vid', FALSE),
    '#options' => $options,
    '#description' => t('Choose the vocabulary to use for restricting product sales by region'),
  );

  return system_settings_form($form);
}

/**
 * Implementation of hook_menu
*/
function uc_restrict_menu(){
  $items['admin/settings/uc-restrict'] = array(
    'access arguments' => array('Configure uc_restrict'),
    'title' => 'UC Restrict',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('uc_restrict_settings_form'),
    'type' => MENU_NORMAL_ITEM,
  );

  $items['clear-postcode'] = array(
    'access arguments' => array('access content'),
    'title' => 'Clear postcode',
    'page callback' => 'uc_restrict_clear_postcode',
    'type' => MENU_CALLBACK,
  );
  $items['manual-quote/%node'] = array(          
    'title' => 'Request a shipping quote',
    'page callback' => 'uc_restrict_manual_quote',
    'access arguments' => array(1),
    'access callback' => 'uc_restrict_access',
    'page arguments' => array(1),
    'type' => MENU_CALLBACK
  );
  return $items;
}

/**
 * Menu access callback
*/
function uc_restrict_access($node) {
  return !empty($node->type) && $node->type == 'product' && user_access('access content');
}

/**
 * Implementation of hook_form_alter
*/
function uc_restrict_form_alter(&$form, $form_state, $form_id) {
  //taxonomy form
  if ($form_id == 'taxonomy_form_term' && isset($form['vid']) &&
      $form['vid']['#value'] == variable_get('uc_restrict_vid', FALSE)) {
    $form['uc_restrict'] = array(
      '#type'         => 'fieldset',
      '#collapsible'  => TRUE,
      '#title'        => t('Shipping restrictions'),
      '#collapsed'    => FALSE,
      '#description'  => t('Control whether products in this category can be sold via the cart')
    );

    $default = '';
    if (isset($form['tid'])) {
      $default = variable_get('uc_restrict_term_pattern_'. $form['tid']['#value'], '');
    }

    $form['uc_restrict']['uc_restrict_postcodes'] = array(
      '#type'          => 'textarea',
      '#title'         => t('Postcode pattern'),
      '#default_value' => $default,
      '#description' => t('Specify a postal code or postal code pattern. <br />
                        Seperate values with a comma<br />
                        Seperate ranges with a hyphen<br />
                        No spaces please</br>
                        Example: 4670,2453-2469'),
      '#required'      => FALSE,
      '#resizable'     => TRUE
    );

    $form['submit']['#weight']++;
   	if ($form['delete']) {
     	$form['delete']['#weight']+=2;
   	}
  }

  //add to cart forms
  if (substr($form_id, 0, strlen('uc_product_add_to_cart_form')) == 'uc_product_add_to_cart_form') {
    $node = $form_state['build_info']['args'][0];
    $vid = variable_get('uc_restrict_vid', FALSE);
    $field_name = 'taxonomy_vocabulary_'. $vid;
    $items = field_get_items('node', $node, $field_name);
    foreach ($items as $delta => $value) {
      $tid = $value['tid'];
      $term = $value['taxonomy_term'];
      $pattern = variable_get('uc_restrict_term_pattern_'. $tid, FALSE);
      if ($pattern) {
        $default = '';
        if ($_SESSION['uc_restrict_postcode']) {
          $default = $_SESSION['uc_restrict_postcode'];
          $pass = _uc_restrict_test_postcode($default, $pattern);
        }
        if ($default)  {
          //we've got their postcode already from a previous form
          if ($pass) {
            //their postcode is ok for this product
            $form['message'] = array(
              '#markup' => t(
                'Checkout OK to postcode @default (<a href="/clear-postcode">Clear</a>)<br />',
                array(
                  '@default' => $default
                )
              )
            );
            $form['submit']['#weight']++;
          }
          else {
            //their postcode is not ok for this product
            $form['message'] = array(
              '#markup' => t(
                'Manual quote required to @default (<a href="/clear-postcode">Clear</a>)<br />',
                array(
                  '@default' => $default
                )
              )
            );
            $form['submit']['#value'] = t('Request a quote');
            $form['#submit'] = array('uc_restrict_add_to_cart_manual_quote');
            $form['submit']['#weight']++;
          }
        }
        else {
          //no postcode, so lets collect it
          $form['tid'] = array(
            '#type'  => 'value',
            '#value' => $tid
          );
          $form['postcode'] = array(
            '#type'          => 'textfield',
            '#title'         => t('Postcode'),
            '#size'          => 30,
            '#description'   => t('Enter your postcode'),
            '#maxlength'     => 6,
            '#required'      => TRUE,
          );
          $form['#validate'][] = 'uc_restrict_add_to_cart_validate';
          $form['submit']['#weight']++;
        }
      }
    }
  }
}

/**
 * Implementation of hook_taxonomy_term_insert
*/
function uc_restrict_taxonomy_term_insert($term) {
  if ($term->vid == variable_get('uc_restrict_vid', FALSE)) {
    variable_set('uc_restrict_term_pattern_'. $term->tid, $term->uc_restrict_postcodes);
  }
}

/**
 * Implementation of hook_taxonomy_term_update
*/
function uc_restrict_taxonomy_term_update($term) {
  if ($term->vid == variable_get('uc_restrict_vid', FALSE)) {
    variable_set('uc_restrict_term_pattern_'. $term->tid, $term->uc_restrict_postcodes);
  }
}


/**
 * Utility to test a postcode against a range
 * @param $postcode int postcode to test
 * @param $pattern int pattern to test against
 * @return boolean
*/
function _uc_restrict_test_postcode($postcode, $pattern) {
  //split on ,
  $codes = explode(',', $pattern);
  $processed = array();
  foreach ($codes as $code) {
    $code = trim($code);
    $ranges = explode('-', $code);
    if (is_array($ranges) && count($ranges) == 2) {
      //we lpad with zeros so 203 is less than 2030 ie 00203 is less than 02030
      $processed[] = array('type' => 'range',
                           'upper' => str_pad(trim($ranges[1]), 15, '0', STR_PAD_LEFT),
                           'lower' => str_pad(trim($ranges[0]), 15, '0', STR_PAD_LEFT));
    }
    else {
      $processed[] = array('type' => 'plain',
                           'value' => str_pad(trim($ranges[0]), 15, '0', STR_PAD_LEFT));
    }
  }
  $target = str_pad($postcode, 15, '0', STR_PAD_LEFT);
  if (is_array($processed) && count($processed)) {
    foreach ($processed as $code) {
      if ($code['type'] == 'range') {
        if ($target <= $code['upper'] && $target >= $code['lower']) {
          return TRUE;
        }
      }
      else {
        if ($target == $code['value']) {
          return TRUE;
        }
      }
    }
  }
  return FALSE;
}

/**
 * Provides the manual quote form page
 *
*/
function uc_restrict_manual_quote($node = FALSE) {
  if (!$node) {
    return drupal_not_found();
  }
  global $user;

  if (!flood_is_allowed('uc_restrict_quote', 10)) {
    $output = t("You cannot send more than %number messages per hour. Please try again later.", array('%number' => 10));
  }
  else {
    drupal_set_message(t("Unfortunately we can't send the product '@product' to your postcode at our normal delivery fee,<br />
                         please complete the form below and we'll organise a precise delivery quote to your location",
                         array('@product' => $node->title)));
    module_load_include('inc', 'contact', 'contact.pages');
    $form_state = array(
      'build_info' => array('node' => $node, 'manual_quote' => TRUE, 'args' => array()),
    );
    $form = drupal_build_form('contact_site_form', $form_state);
    return $form;
  }

  return $output;
}

/**
 * Menu callback to clear SESSION var and then redirect
*/
function uc_restrict_clear_postcode() {
  unset($_SESSION['uc_restrict_postcode']);
  drupal_goto($_SERVER['HTTP_REFERER']);
}

/**
 * Validation handler for postcode entry
*/
function uc_restrict_add_to_cart_validate($form, $form_state) {
  $form_values = $form_state['values'];
  $tid = $form_values['tid'];
  if ($tid) {
    $pattern = variable_get('uc_restrict_term_pattern_'. $tid, FALSE);
    if ($pattern) {
      $default = $form_values['postcode'];
      $_SESSION['uc_restrict_postcode'] = $form_values['postcode'];
      $pass = _uc_restrict_test_postcode($default, $pattern);
      if (!$pass && $default) {
        drupal_goto('manual-quote/'. $form_values['nid']);
      }
    }
  }
  else {
    return drupal_not_found();
  }
}

/**
 * Form builder for manual contact
*/
function uc_restrict_form_contact_site_form_alter(&$form, $form_state) {
  if (!empty($form_state['build_info']['manual_quote']) &&
    ($node = $form_state['build_info']['node'])) {
    
    unset($form['contact_information']);
    $form['subject']['#default_value'] = t('Please provide a delivery quote');
    $form['message']['#default_value'] = t("Hi\nI'd like to buy your product '@product' (!url), my postcode is @postcode - can you please send me a delivery quote?\nThanks", array(
        '!url' => url('node/'. $node->nid, array('absolute' => TRUE)),
        '@product' => $node->title,
        '@postcode' => $_SESSION['uc_restrict_postcode']
      ));
  }
}

/**
 * Tell workflow about the conditions
 * @todo port this to rules
*/
function uc_restrict_condition_info() {
  $order_arg = array('#entity' => 'order', '#label' => t('Order'));

  $conditions['uc_restrict_delivery_postal_code'] = array(
    '#label' => t("Check an order's delivery postal code"),
    '#arguments' => array('order' => $order_arg),
    '#description' => t('Returns TRUE if the shipping postal code matches the specified pattern.'),
    '#module' => t('UC Restrict: Shipping address'),
  );

  return $conditions;
}

/**
 * Check an order's delivery postal code.
 * @todo port this to rules
*/
function uc_restrict_delivery_postal_code($order, $settings) {
  $pattern = $settings['pattern'];

  return _uc_restrict_test_postcode($order->delivery_postal_code, $pattern);
}

/**
 * Collect the pattern
 * @todo port this to rules
*/
function uc_restrict_delivery_postal_code_form($settings = array()) {
  $form['pattern'] = array(
      '#type'          => 'textarea',
      '#title'         => t('Postcode pattern'),
      '#default_value' => $settings['pattern'],
      '#description' => t('Specify a postal code or postal code pattern. <br />
                        Seperate values with a comma<br />
                        Seperate ranges with a hyphen<br />
                        No spaces please</br>
                        Example: 4670,2453-2469'),
      '#required'      => FALSE,
      '#resizable'     => TRUE
    );

  return $form;
}

/**
 * Submit the form
 * @todo port this to rules
*/
function uc_restrict_delivery_postal_code_submit($form_id, $form_values) {
  return array('pattern' => $form_values['pattern']);
}
